import * as React from 'react';
import * as CSSModules from 'react-css-modules';

const styles = require('./header-divider.css');

export interface HeaderDividerProps {
    text: string;
}

class HeaderDivider extends React.Component<HeaderDividerProps, void> {
    render(): JSX.Element {
        return (
            <div styleName="header_divider_container">
                <span styleName="header_divider_text">{this.props.text}</span>
                <img styleName="header_divider_image" src="/assets/images/header-divider.png" alt="Header Divider" />
            </div>
        );
    }
}

export default CSSModules(HeaderDivider, styles, { allowMultiple: true });
