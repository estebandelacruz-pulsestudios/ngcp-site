import * as React from 'react';
import * as CSSModules from 'react-css-modules';
import { Link } from 'react-router-dom';

const styles = require('./board-member-bubble.css');

export interface BoardMemeberBubbleProps {
    name: string;
    title: string;
    linkTo: string;
}

export interface BoardMemeberBubbleState {
    isMouseHovered: boolean;
}

class BoardMemberBubble extends React.Component<BoardMemeberBubbleProps, BoardMemeberBubbleState> {

    componentWillMount(): void {
        this.setState({
            isMouseHovered: false
        });
    }

    render(): JSX.Element {
        return (
            <div
             styleName="board_member_bubble_container"
             onMouseEnter={this._triggerMouseHover()}
            >
                <div styleName={this.state.isMouseHovered ? 'bubble_visible' : 'bubble_hidden'}>
                    <Link to={this.props.linkTo}>
                        {this._displayMemberImage(this.props.name)}
                        <div styleName="board_member_bubble_text_box">
                            <span styleName="board_member_bubble_name">{this.props.name}</span>
                            <span styleName="board_member_bubble_title">{this.props.title}</span>
                        </div>
                    </Link>
                </div>
            </div>
        );
    }

    private _triggerMouseHover(): any {
        console.log('trigger');
        if (this.state.isMouseHovered) {
            this.setState({
                isMouseHovered: true
            });
        } else {
            this.setState({
                isMouseHovered: false
            });
        }
    }

    private _displayMemberImage(name: string): JSX.Element {

        switch (name) {
            case 'Fletcher Rudd':
                return (
                    <img
                        styleName="board_member_bubble_image"
                        src="/assets/images/team_pics/fletcher_rudd_portrait.png"
                        alt="Fletcher Rudd Picture"
                    />
                );
            case 'Thao Pham':
                return (
                    <img
                        styleName="board_member_bubble_image"
                        src="/assets/images/team_pics/thao_portrait.png"
                        alt="Thao Pham Picture"
                    />
                );
            case 'Frank Sanchez':
                return (
                    <img
                        styleName="board_member_bubble_image"
                        src="/assets/images/team_pics/frank_portrait.png"
                        alt="Frank Sanchez Picture"
                    />
                );
            case 'William Walker':
                return (
                    <img
                        styleName="board_member_bubble_image"
                        src="/assets/images/team_pics/will_walker_portrait.png"
                        alt="William Walker Picture"
                    />
                );
            case 'Esteban De La Cruz':
                return (
                    <img
                        styleName="board_member_bubble_image"
                        src="/assets/images/team_pics/esteban_portrait.png"
                        alt="Esteban De La Cruz Picture"
                    />
                );
            case 'Ahmed Quadri':
                return (
                    <img
                        styleName="board_member_bubble_image"
                        src="/assets/images/team_pics/ahmed_quadri_portrait.png"
                        alt="Ahmed Quadri Picture"
                    />
                );
        }

        return <div/>;
    }
}

export default CSSModules(BoardMemberBubble, styles, { allowMultiple: true });
