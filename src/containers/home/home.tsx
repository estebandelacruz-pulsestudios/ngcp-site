import * as React from 'react';
import * as CSSModules from 'react-css-modules';
// import { Link } from 'react-router-dom';

const styles = require('./home.css');

const Home = () =>
    <article>
        <div styleName="cover_image">
            <h1 styleName="cover_text">NGCP</h1>
        </div>
        <div styleName="section_one_container">
            <div styleName="slideshow_image_container">
                <img styleName="slideshow_image" src="http://via.placeholder.com/350x400" alt="Slideshow Image" />
            </div>
            <div styleName="slideshow_description_container">
                <p styleName="slideshow_description">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Ut id lorem sodales, tincidunt enim varius, faucibus eros. Proin vel lectus odio. Donec
                    blandit libero ac velit lobortis, in volutpat dui volutpat. Morbi lacinia lectus facilisis,
                    suscipit nisl sed, rutrum est. Vestibulum dapibus urna justo, finibus vestibulum diam
                    feugiat maximus. Nulla tristique sem in tellus dignissim, vitae dictum eros interdum. Morbi
                    ultrices, lectus eget gravida consectetur, diam nisi interdum risus, laoreet efficitur mi
                     nulla a nisi. Fusce ut lectus accumsan, facilisis nulla et, fermentum massa. </p>
                <button styleName="learn_more_button">Learn More</button>
            </div>
        </div>
    </article>;

export default CSSModules(Home, styles, { allowMultiple: true });
