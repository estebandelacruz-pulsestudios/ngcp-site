import * as React from 'react';
import * as CSSModules from 'react-css-modules';
import { Link } from 'react-router-dom';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Home from './home';
import OurMusicians from './team';

const styles = require('./root.css');

class Root extends React.Component<{}, void> {
    render(): JSX.Element {
        return (
            <Router>
                <div>
                    <main>
                        <div>
                            <div styleName="header_inner_container">
                                <Link styleName="header_item" to="/events">Events</Link>
                                <Link styleName="header_item" to="/contact">Contact</Link>
                                <Link to="/"><img styleName="header_ngcp_logo" src="" alt="NGCP Logo"/></Link>
                                <Link styleName="header_item" to="/team">About</Link>
                                <Link styleName="header_item" to="/Members">Members</Link>
                            </div>
                        </div>
                        <Route
                            render={() => {
                                if (typeof window !== 'undefined') {
                                    window.scrollTo(0, 0);
                                }

                                return (
                                    <Switch>
                                        <Route
                                            exact
                                            path="/"
                                            component={Home}
                                        />
                                        <Route
                                            exact
                                            path="/team"
                                            component={OurMusicians}
                                        />
                                    </Switch>
                                );
                            }}
                        />
                    </main>
                </div>
            </Router>
        );
    }
}

export default CSSModules(Root, styles, { allowMultiple: true });
