import * as React from 'react';
import * as CSSModules from 'react-css-modules';

import BoardMember from 'components/board-member';
import HeaderDivider from 'components/header-divider';

const styles = require('./team.css');

const OurMusicians = () =>
    <article>
        <div>
            <HeaderDivider text="Board Members" />
            <p styleName="board_members_body">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Ut id lorem sodales, tincidunt enim varius, faucibus eros. Proin vel lectus odio. Donec
                blandit libero ac velit lobortis, in volutpat dui volutpat. Morbi lacinia lectus facilisis,
                suscipit nisl sed, rutrum est. Vestibulum dapibus urna justo, finibus vestibulum diam
                feugiat maximus. Nulla tristique sem in tellus dignissim, vitae dictum eros interdum. Morbi
                ultrices, lectus eget gravida consectetur, diam nisi interdum risus, laoreet efficitur mi
                nulla a nisi. Fusce ut lectus accumsan, facilisis nulla et, fermentum massa. </p>
            <p styleName="board_members_body red_body">Click on a portrait to learn more about our board members.</p>
            <div styleName="board_members_bubble_container">
                <BoardMember name="Fletcher Rudd" title="President &amp; Artistic Director" linkTo="/example-profile"/>
                <BoardMember name="Thao Pham" title="President" linkTo="/example-profile"/>
                <BoardMember name="Frank Sanchez" title="President" linkTo="/example-profile"/>
                <BoardMember name="William Walker" title="President" linkTo="/example-profile"/>
                <BoardMember name="Esteban De La Cruz" title="President" linkTo="/example-profile"/>
                <BoardMember name="Ahmed Quadri" title="President" linkTo="/example-profile"/>
            </div>
            <div>
                <HeaderDivider text="Our Musicians" />
                <p styleName="board_members_body">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Ut id lorem sodales, tincidunt enim varius, faucibus eros. Proin vel lectus odio. Donec
                    blandit libero ac velit lobortis, in volutpat dui volutpat. Morbi lacinia lectus facilisis,
                    suscipit nisl sed, rutrum est. </p>
                <div styleName="our_musicians_container">
                    <div styleName="our_musicians_section">
                        <img
                            styleName="our_musicians_section_header"
                            src="/assets/images/violin-I-header.png"
                            alt="Treble Clef"
                        />
                        <p>First Name</p>
                        <p>First Name</p>
                        <p>First Name</p>
                        <p>First Name</p>
                        <p>First Name</p>
                    </div>
                    <div styleName="our_musicians_section">
                        <img
                            styleName="our_musicians_section_header"
                            src="/assets/images/violin-II-header.png"
                            alt="Treble Clef"
                        />
                        <p>First Name</p>
                        <p>First Name</p>
                        <p>First Name</p>
                        <p>First Name</p>
                        <p>First Name</p>
                    </div>
                    <div styleName="our_musicians_section">
                        <img
                            styleName="our_musicians_section_header"
                            src="/assets/images/viola-header.png"
                            alt="Alto Clef"
                        />
                        <p>First Name</p>
                        <p>First Name</p>
                        <p>First Name</p>
                        <p>First Name</p>
                        <p>First Name</p>
                    </div>
                    <div styleName="our_musicians_section">
                        <img
                            styleName="our_musicians_section_header"
                            src="/assets/images/cello-header.png"
                            alt="Bass Clef"
                        />
                        <p>First Name</p>
                        <p>First Name</p>
                        <p>First Name</p>
                        <p>First Name</p>
                        <p>First Name</p>
                    </div>
                    <div styleName="our_musicians_section">
                        <img
                            styleName="our_musicians_section_header"
                            src="/assets/images/bass-header.png"
                            alt="Bass Clef"
                        />
                        <p>First Name</p>
                        <p>First Name</p>
                        <p>First Name</p>
                        <p>First Name</p>
                        <p>First Name</p>
                    </div>
                </div>
            </div>
        </div>
    </article>;

export default CSSModules(OurMusicians, styles, { allowMultiple: true });
