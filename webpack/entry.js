const path = require('path');
const isProduction = require('./is-production');

const envEntry = isProduction ? [] : [
    'es6-symbol/implement',
    'react-hot-loader/patch',
    'webpack-dev-server/client?http://localhost:8080',
    'webpack/hot/only-dev-server',
];

module.exports = [
    ...envEntry,
    'babel-polyfill',
    'whatwg-fetch',
    path.resolve('./src/index.tsx'),
];